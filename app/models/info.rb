class Info < ApplicationRecord
  extend FriendlyId
  belongs_to :category

  validates :title, :date, :presence => true
  friendly_id :title, use: :slugged
end
