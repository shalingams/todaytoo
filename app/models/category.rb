class Category < ApplicationRecord
  extend FriendlyId
  has_many :info

  validates :name, :presence => true
  friendly_id :name, use: :slugged
end
