class Question < ApplicationRecord
  extend FriendlyId
  belongs_to :question_type
  has_many :answers
  accepts_nested_attributes_for :answers, :allow_destroy => true

  validates :question, :presence => true
  friendly_id :question, use: :slugged

end
