class QuestionType < ApplicationRecord
  extend FriendlyId
  has_many :questions

  validates :name, :presence => true
  friendly_id :name, use: :slugged

end
