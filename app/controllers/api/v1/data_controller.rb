class Api::V1::DataController < ApplicationController
  before_action :cors_preflight_check
  after_action :cors_set_access_control_headers

  def cors_set_access_control_headers
    headers['Access-Control-Allow-Origin'] = '*'
    headers['Access-Control-Allow-Methods'] = 'POST, GET, PUT, DELETE, OPTIONS'
    headers['Access-Control-Allow-Headers'] = 'Origin, Content-Type, Accept, Authorization, Token'
    headers['Access-Control-Max-Age'] = "1728000"
  end

  def cors_preflight_check
    if request.method == 'OPTIONS'
      headers['Access-Control-Allow-Origin'] = '*'
      headers['Access-Control-Allow-Methods'] = 'POST, GET, PUT, DELETE, OPTIONS'
      headers['Access-Control-Allow-Headers'] = 'X-Requested-With, X-Prototype-Version, Token, Content-Type'
      headers['Access-Control-Max-Age'] = '1728000'

      render text: '', content_type: 'text/plain'
    end
  end

  def handle_options_request
    render :text => '', :content_type => 'text/plain'
  end

  def index
    if params[:day].nil? && params[:month].nil?
      @infos = Info.order("RANDOM()").limit(10)
    elsif params[:day].nil? && !params[:month].nil?
      desired_month = params[:month]
      @infos = Info.where('extract(month from date) = ?', desired_month)
    elsif params[:month].nil? && !params[:day].nil?
      desired_day = params[:day]
      @infos = Info.where('extract(day  from date) = ?', desired_day)
    elsif !params[:month].nil? && !params[:day].nil?
      desired_day = params[:day]
      desired_month = params[:month]
      @infos = Info.where('extract(month from date) = ? AND extract(day  from date) = ?', desired_month, desired_day)
    end
    render json: {infos: @infos}
  end

  def show
    @infos = Info.find(params[:id])
    render json: {infos: @infos}
  end

end
