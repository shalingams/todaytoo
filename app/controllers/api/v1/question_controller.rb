class Api::V1::QuestionController < ApplicationController
  before_action :cors_preflight_check
  after_action :cors_set_access_control_headers

  def cors_set_access_control_headers
    headers['Access-Control-Allow-Origin'] = '*'
    headers['Access-Control-Allow-Methods'] = 'POST, GET, PUT, DELETE, OPTIONS'
    headers['Access-Control-Allow-Headers'] = 'Origin, Content-Type, Accept, Authorization, Token'
    headers['Access-Control-Max-Age'] = "1728000"
  end

  def cors_preflight_check
    if request.method == 'OPTIONS'
      headers['Access-Control-Allow-Origin'] = '*'
      headers['Access-Control-Allow-Methods'] = 'POST, GET, PUT, DELETE, OPTIONS'
      headers['Access-Control-Allow-Headers'] = 'X-Requested-With, X-Prototype-Version, Token, Content-Type'
      headers['Access-Control-Max-Age'] = '1728000'

      render text: '', content_type: 'text/plain'
    end
  end

  def handle_options_request
    render :text => '', :content_type => 'text/plain'
  end

  def index
    @questionTypes = QuestionType.all
    render json: {questionTypes: @questionTypes}
  end

  def show
    @questionType = QuestionType.friendly.find(params[:id])
    question_ids = params[:question_ids]
    if question_ids.nil?
      @question = @questionType.questions.first
    else
      @question = @questionType.questions.where('id NOT IN (?)', question_ids).order("RANDOM()").limit(10)
    end

    render json: {questionType: @questionType, question: @question}
  end
end
