require 'base64'
ActiveAdmin.register Info do
  permit_params :title, :date, :category_id, :description, :more_info, :day_of_year, :image

  controller do

    def show
      @info = Info.friendly.find(params[:id])
    end

    def create
      @info = Info.new(permitted_params[:info])
      if(params['info']['image'])
        @info.image = Base64.encode64(open(params['info']['image'].tempfile.path) { |io| io.read })
      end
      super
    end

    def edit
      @info = Info.friendly.find(params[:id])
      super
    end

    def update
      @info = Info.friendly.find(params[:id])
      super
    end

  end
  form do |f|
    inputs 'Details' do
      input :category
      input :title
      input :date, as: :datepicker
      input :description
      input :more_info
      input :day_of_year
      input :image, as: :file
    end
    actions
  end

  index do
    selectable_column
    id_column
    column :title
    column :date
    column :category
    column :description
    column :image, sortable: false do |info|
      image_tag("data:image/jpeg;base64,#{info.image}", size: "50x50")
    end
    actions
  end


  show do
    attributes_table do
      row :category
      row :title
      row :date
      row :description
      row :more_info
      row :day_of_year
      row :image do
        image_tag("data:image/jpeg;base64,#{info.image}")
      end
    end
  end

end
