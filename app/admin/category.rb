ActiveAdmin.register Category do

  permit_params :name, :image

  controller do
    def show
      @category = Category.friendly.find(params[:id])
    end

    def create
      @category = Category.new(permitted_params[:category])
      @category.image = Base64.encode64(open(params['category']['image'].tempfile.path) { |io| io.read })
      super
    end

    def edit
      @category = Category.friendly.find(params[:id])
      super
    end

    def update
      @category = Category.friendly.find(params[:id])
      super
    end
  end

  form do |f|
    inputs 'Details' do
      input :name
      input :image, as: :file
    end
    actions
  end

  index do
    selectable_column
    id_column
    column :name
    column :image, sortable: false do |category|
      image_tag("data:image/jpeg;base64,#{category.image}", size: "50x50")
    end
    actions
  end

  show do
    attributes_table do
      row :name
      row :image do
        image_tag("data:image/jpeg;base64,#{category.image}")
      end
    end
  end

end
