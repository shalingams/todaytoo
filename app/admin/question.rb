ActiveAdmin.register Question do

  permit_params :question, :question_type_id, answers_attributes: [:answer]

  controller do

    def show
      @question = Question.friendly.find(params[:id])
    end

    def edit
      @question = Question.friendly.find(params[:id])
      super
    end

    def update
      @question = Question.friendly.find(params[:id])
      super
    end

  end

  form do |f|
    inputs 'Question' do
      input :question_type
      input :question
      inputs 'Answers (Second answer must be right answer)' do
        has_many :answers do |a|
          a.input :answer
        end
      end
    end

    actions
  end

  index do
    selectable_column
    id_column
    column :question
    column :question_type
    actions
  end


  # show do
  #   attributes_table do
  #     row :question
  #     row :answers do
  #       question.answers.each do |ans|
  #         ans.answer
  #       end
  #     end
  #   end
  # end

  show do
    panel "Question" do
      table_for question do
        column :question
        column :question_type
      end
    end
    panel "Answers" do
      table_for question.answers do
        column :answer
      end
    end
  end


end
