ActiveAdmin.register QuestionType do

  permit_params :name

  controller do

    def show
      @question_type = QuestionType.friendly.find(params[:id])
    end

    def edit
      @question_type = QuestionType.friendly.find(params[:id])
      super
    end

    def update
      @question_type = QuestionType.friendly.find(params[:id])
      super
    end

  end
  form do |f|
    inputs 'Details' do
      input :name
    end
    actions
  end

  index do
    selectable_column
    id_column
    column :name
    actions
  end


  show do
    attributes_table do
      row :name
    end
  end

end
