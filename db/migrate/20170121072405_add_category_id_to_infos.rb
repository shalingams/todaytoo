class AddCategoryIdToInfos < ActiveRecord::Migration[5.0]
  def change
    add_column :infos, :category_id, :integer
  end
end
