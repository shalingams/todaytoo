class CreateQuestionTypes < ActiveRecord::Migration[5.0]
  def change
    create_table :question_types do |t|
      t.string :name
      t.string :slug

      t.timestamps
    end
    add_index :question_types, :slug, unique: true
  end
end
