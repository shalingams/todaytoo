class CreateQuestions < ActiveRecord::Migration[5.0]
  def change
    create_table :questions do |t|
      t.string :question
      t.integer :question_type_id
      t.string :slug

      t.timestamps
    end
    add_index :questions, :slug, unique: true
  end
end
