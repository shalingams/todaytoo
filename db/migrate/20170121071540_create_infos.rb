class CreateInfos < ActiveRecord::Migration[5.0]
  def change
    create_table :infos do |t|
      t.date :date
      t.text :description
      t.text :more_info
      t.string :title
      t.text :image
      t.integer :day_of_year
      t.string :slug

      t.timestamps
    end
  end
end
