Rails.application.routes.draw do

  devise_for :admin_users, ActiveAdmin::Devise.config
  devise_for :users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  namespace :api do
    namespace :v1 do
      resources :data, only: [:show,:index]
      resources :question, only: [:show,:index]
    end
  end

  root 'home#index'
end
